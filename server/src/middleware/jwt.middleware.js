const jwt = require('jsonwebtoken')
const secret = 'devSquad'

const createToken = obj => new Promise( resolve  => resolve(jwt.sign(obj, secret)) )

const decodeToken = token => new Promise((resolve, reject) => {
    const decodedToken = jwt.decode(token, secret)

    if(decodedToken === null) return reject({message: 'Invalid token.'})

    return resolve(decodedToken)
})

module.exports = {
    createToken,
    decodeToken
}