module.exports = function(app) {
    const cardControllers = require('../controllers/card.controller')

    app.post('/api/card/create/:board', cardControllers.createCard)
    app.post('/api/card/completed/:board', cardControllers.completeCard)
    app.post('/api/card/name/:board', cardControllers.nameCard)
    app.post('/api/card/remove/:board', cardControllers.removeCard)
    app.post('/api/card/assign/:board', cardControllers.assignCard)
    app.post('/api/card/changing/:board', cardControllers.changingCard)
}