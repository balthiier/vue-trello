module.exports = function(app) {
    const boardControllers = require('../controllers/board.controller')
    const jwtMiddleware = require('../middleware/jwt.middleware')

    app.post('/api/board/create', boardControllers.createBoard)
    app.post('/api/board/list', boardControllers.showBoards)
    app.post('/api/board/delete', boardControllers.deleteBoard)

    app.post('/api/board/showguestboards', boardControllers.showBoardsGuest)
    app.post('/api/board/guestList/:board', boardControllers.guestList)
    app.put('/api/board/guestList/:board', boardControllers.addGuests)

    app.get('/api/board/getBoard/:boardID', boardControllers.getBoard)
}