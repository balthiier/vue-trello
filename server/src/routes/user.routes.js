module.exports = function(app) {
    const userControllers = require('../controllers/user.controller')

    app.post('/api/user/create', userControllers.createUser)
    app.post('/api/user/authenticate', userControllers.authenticateUser)
    app.get('/confirmemail/:token', userControllers.confirmUserEmail)
    app.post('/recoverpass/:email', userControllers.recoverPassword)
    app.post('/resetpass/:token', userControllers.resetPassword)
}