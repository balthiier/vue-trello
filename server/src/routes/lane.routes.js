module.exports = function(app) {
    const laneControllers = require('../controllers/lane.controller')

    app.post('/api/lane/create/:board', laneControllers.createLane)
    app.post('/api/lane/remove/:board', laneControllers.removeLane)
    app.post('/api/lane/update/:board', laneControllers.updateLaneName)
}