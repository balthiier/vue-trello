module.exports = [
   'All we have to decide is what to do with the time that is given us.',
   'I will not say: do not weep; for not all tears are an evil.',
   'Many that live deserve death. And some that die deserve life. Can you give it to them? Then do not be too eager to deal out death in judgement.',
   'For even the very wise cannot see all ends.',
   'Fly you fools.'
]