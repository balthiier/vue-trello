module.exports = function(app) {
    const morgan = require('morgan')
    const bodyParser = require('body-parser')
    const expressJwt = require('express-jwt')

    // jwt token secret
    app.set('secret', 'devSquad')
    // setting a limit to body parser middleware and to get body REQ & RES easy
    app.use(bodyParser.urlencoded({extended: false, limit: '1mb'}))
    app.use(bodyParser.json())
    // using morgan to log all HTTP requests and responses
    app.use(morgan('tiny'))
    // set json web tokens to make controlled api usage and security purpose
    app.use(expressJwt({secret: app.get('secret')}).unless({path: [/favicon.ico/, '/', '/api/user/authenticate', '/api/user/create', /\/recoverpass\/\S*/gim, /\/confirmemail\/\S*/gim, /\/resetpass\/\S*/gim]}))

    // configure to enable CORS for port 8080
    app.use((req, res, next) => {
        res.header("Access-Control-Allow-Origin", "http://localhost:8080")
        res.header("Access-Control-Allow-Methods", "GET, PUT, POST, DELETE, PATCH")
        res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, Authorization")
        next()
    })

    // enabling access to the routes
    require('../routes/user.routes')(app)
    require('../routes/lane.routes')(app)
    require('../routes/card.routes')(app)
    require('../routes/board.routes')(app)
}