const mongoConnection = {
    init: function() {
        const { db } = require('./server.config')
        const mongoose = require('mongoose')
        mongoose.Promise = Promise

        mongoose.connect(db, {keepAlive: 120})
            .then(connection => console.log(`mongoDB connected at port ${connection.connections[0].port}`))
            .catch(err => console.log(err.message))

        return {mongoose}
    }
}

module.exports = Object.create(mongoConnection).init()