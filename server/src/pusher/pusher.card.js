const pusher = require('../config/pusher.config')

const createCard = (boardId, data) => {
    pusher.trigger(`my-channel-${boardId}`, 'card-create', data)
}

const cardCompleted = (boardId, data) => {
    pusher.trigger(`my-channel-${boardId}`, 'card-completed', data)
}

const nameCard = (boardId, data) => {
    pusher.trigger(`my-channel-${boardId}`, 'card-name', data)
}

const removeCard = (boardId, data) => {
    pusher.trigger(`my-channel-${boardId}`, 'card-remove', data)
}

const assignCard = (boardId, data) => {
    pusher.trigger(`my-channel-${boardId}`, 'card-assign', data)
}

const changingCard = (boardId, data) => {
    pusher.trigger(`my-channel-${boardId}`, 'card-changing', data)
}

module.exports = {
    createCard,
    cardCompleted,
    nameCard,
    removeCard,
    assignCard,
    changingCard
}