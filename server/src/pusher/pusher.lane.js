const pusher = require('../config/pusher.config')

const createLane = (boardId, data) => {
    pusher.trigger(`my-channel-${boardId}`, 'lane-create', { data })
}

const deleteLane = (boardId, data) => {
    pusher.trigger(`my-channel-${boardId}`, 'lane-delete', { data })
}

const updateLaneName = (boardId, data) => {
    pusher.trigger(`my-channel-${boardId}`, 'lane-update', { data })
}

module.exports = {
    createLane,
    deleteLane,
    updateLaneName
}