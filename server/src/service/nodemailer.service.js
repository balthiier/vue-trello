const nodemailer = require('nodemailer')

const nodeMailerService = {
    init: function(name, email, token, isConfirmEmail) {
        nodemailer.createTestAccount((err, account) => {
            /**-email credentials-
             * email: vuetrellodev@gmail.com
             * pass: vuetrellodev123!
            */

            // create reusable transporter object using the default SMTP transport
            const transporter = nodemailer.createTransport({
                host: 'smtp.ethereal.email',
                service: 'gmail',
                auth: {
                    user: 'vuetrellodev@gmail.com',
                    pass: 'vuetrellodev123!'
                }
            })

            const htmlTemplate = {
                confirmEmail: {
                    title: 'Vue Trello Account Activation',
                    content: `<b>Hello ${name}, <a href="http://localhost:3000/confirmemail/${token}">click here</a> to activate the user account to start using full vue trello service.</b>`
                },
                resetPass: {
                    title: 'Vue Trello Reset Password',
                    content: `<b>Hello ${name}, <a href="http://localhost:8080/resetpass/${token}">click here</a> to reset your password.</b>`
                }
            }
        
            // setup email data with unicode symbols
            let mailOptions = {
                from: '"Vue Trello Team" <vuetrellodev@gmail.com>', // sender address
                to: email, // list of receivers
                subject: isConfirmEmail === true ? htmlTemplate.confirmEmail.title : htmlTemplate.resetPass.title, // Subject line
                html: isConfirmEmail === true ? htmlTemplate.confirmEmail.content : htmlTemplate.resetPass.content // html body
            }
        
            // send mail with defined transport object
            transporter.sendMail(mailOptions, (error, info) => {
                if (error) return console.log(error)
                
                console.log('Message sent to: %s', email)
            })
        })
    }
}

module.exports = (name, email, token, isConfirmEmail) => 
    Object.create(nodeMailerService).init(name, email, token, isConfirmEmail)