const boardModel = require('../models/board.model')
const userModel = require('../models/user.model')
const laneModel = require('../models/lane.model')

const createBoard = (req, res) => {
    const { boardName, id } = req.body

    const defaultLanes = ['TODO', 'PROGRESS', 'DONE']
    let promises = []

    defaultLanes.map(lane => {
        promises.push(new Promise((resolve, reject) => {
           laneModel.create({laneName: lane})
            .then(lane => resolve(lane))
        }))
    })

    Promise.all(promises)
        .then(lanes => {
            lanesId = lanes.map(lane => lane._id)
        
            boardModel.create({boardName, author: id, lanes: lanesId, guests: [id] })
                .then(obj => res.send(obj))
                .catch(err => res.send(err))
        })
}

const showBoards = (req, res) => {
    const { id } = req.body

    boardModel.find({author: id}).populate('author', '-password').exec((err, boards) => {
        if(!boards) return res.status(404).send({message: 'no boards yet, create one.'})

        return res.status(200).send(boards)
    })
}

const deleteBoard = (req, res) => {
    const { id } = req.body
    
    boardModel.findOne({_id: id}, (err, board) => {
        if(err) return res.send({message: 'Something went wrong and the board was not deleted.'})

        board.remove()

        return res.send({message: 'Board deleted with success.'})
    })
}

const showBoardsGuest = (req, res) => {
    const { id } = req.body

    boardModel.find({guests: id}).populate('author', '-password').exec((err, boards) => {
        if(!boards) return res.status(404).send({message: 'no boards yet, create one.'})

        return res.status(200).send(boards)
    })
}

const guestList = (req, res) => {
    const { id } = req.body

    userModel.find({}, '-password -emailConfirmed', (err, guests) => {
        if(err) throw err

        // get all users unless the caller of that request        
        const filteredGuests = guests.filter(guest => guest._id.toString() !== id)

        return res.status(200).send({guests: filteredGuests})        
    })
}

const addGuests = (req, res) => {
    const { board } = req.params
    const { guests } = req.body
    
    // get all users
    userModel.find({}, '_id, name, email', (err, users) => {
        if(err) throw err

        const flatUsers = guests.map(guest => guest._id)

        boardModel.findOneAndUpdate({ _id: board }, {$addToSet: {guests: flatUsers}}, (err, updateBoard) => {
            if(err) throw err

            return res.status(200).send({message: 'guests invited with success!'})
        })
    })
}

const getBoard = (req, res) => {
    const { boardID } = req.params

    boardModel.findOne({_id: boardID})
    .populate('author, lanes, guests', '-password -emailConfirmed')
    .populate({
        path: 'lanes',
        populate: {
            path: 'tasks',
            component: 'Card',
            populate: {
                path: 'assignedTo',
                select: '-password -emailConfirmed',
                component: 'Card'
            }
        }
    })
    .exec((err, board) => {
        if(err) throw err
        
        return res.status(200).send(board)
    })
}

module.exports = {
    createBoard,
    showBoards,
    deleteBoard,

    showBoardsGuest,
    guestList,
    addGuests,
    getBoard
}