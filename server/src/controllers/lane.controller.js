const laneModel = require('../models/lane.model')
const boardModel = require('../models/board.model')
const pusherLane = require('../pusher/pusher.lane')

const createLane = (req, res) => {
    const { board } = req.params
    const { laneName } = req.body

    laneModel.create({laneName})
        .then(newLane => {
            boardModel.findByIdAndUpdate(board, {$push: {lanes: newLane._id}}, err => {
                if(err) throw err

                pusherLane.createLane(board, newLane)
    
                return res.status(200).send({status: res.statusCode})
            })
            
        })
        .catch(err => res.send(err))
}

const updateLaneName = (req, res) => {
    const { board } = req.params
    const { _id, laneName } = req.body

    laneModel.findByIdAndUpdate(_id, { laneName }, (err, updatedLane) => {
        if(err) throw err

        pusherLane.updateLaneName(board, {...updatedLane._doc, laneName})

        return res.status(200).send({status: res.statusCode})  
    })
}

const removeLane = (req, res) => {
    const { board } = req.params
    const { _id, laneSelected } = req.body

    laneModel.findByIdAndRemove(_id, (err, deletedLane) => {
        if(err) throw err
        
        laneModel.findByIdAndUpdate(laneSelected._id, {$push: {tasks: deletedLane.tasks}}, (err, leftLane) => {
            if(err) throw err
        
            pusherLane.deleteLane(board, deletedLane)
    
            return res.status(200).send({status: res.statusCode})  
        })

    })
}

module.exports = {
    createLane,
    removeLane,
    updateLaneName
}