const cardModel = require('../models/card.model')
const laneModel = require('../models/lane.model')
const pusherCard = require('../pusher/pusher.card')

const createCard = (req, res) => {
    const { board } = req.params
    const { _id, name } = req.body

    cardModel.create({ name })
        .then(newCard => {
            laneModel.findByIdAndUpdate(_id, {$push: {tasks: newCard._id}}, err => {
                if(err) throw err

                pusherCard.createCard(board, {card: newCard, laneId: _id})
    
                return res.status(200).send({status: res.statusCode})
            })
            
        })
        .catch(err => res.send(err))
}

const completeCard = (req, res) => {
    const { board } = req.params    
    const { _id, bool, laneId } = req.body

    cardModel.findByIdAndUpdate(_id, {$set: {completed: bool}}, (err, card) => {
        if(err) throw err

        pusherCard.cardCompleted(board, {_id: card._id, completed: bool, laneId})

        return res.status(200).send({status: res.statusCode})        
    })
}

const nameCard = (req, res) => {
    const { board } = req.params    
    const { _id, name, laneId } = req.body

    cardModel.findByIdAndUpdate(_id, {$set: {name: name}}, (err, card) => {
        if(err) throw err

        pusherCard.nameCard(board, {_id: card._id, name, laneId})

        return res.status(200).send({status: res.statusCode})        
    })
}

const removeCard = (req, res) => {
    const { board } = req.params    
    const { _id, cardPosition, laneId } = req.body

    cardModel.findOne({_id}, (err, card) => {
        if(err) throw err

        card.remove()
        pusherCard.removeCard(board, {_id: card._id, cardPosition, laneId})

        return res.status(200).send({status: res.statusCode})        
    })
}

const assignCard = (req, res) => {
    const { board } = req.params    
    const { _id, assignedTo, laneId } = req.body

    cardModel.findByIdAndUpdate({_id}, {$set: {assignedTo: assignedTo === 'none' ? null : assignedTo }}, (err, card) => {
        if(err) throw err

        pusherCard.assignCard(board, {_id, assignedTo, laneId, user: assignedTo})

        return res.status(200).send({status: res.statusCode})        
    })
}

const changingCard = (req, res) => {
    const { board } = req.params    
    const { cardId, laneFrom, laneTo } = req.body

    new Promise(resolve => {
        cardModel.findById(cardId).populate('assignedTo').exec((err, card) => {
            if(err) throw err

            laneModel.findByIdAndUpdate(laneFrom, {$pull: {tasks: cardId}}, err => {
                if(err) throw err
    
                resolve(card)
            })
        })
    })
    .then(card => {
        laneModel.findByIdAndUpdate(laneTo, {$push: {tasks: cardId}}, (err, newLane) => {
            if(err) throw err

            pusherCard.changingCard(board, {cardId, laneFrom, laneTo, newLane, card})

            return res.status(200).send({status: res.statusCode})  
        })
    })
}



module.exports = {
    createCard,
    completeCard,
    nameCard,
    removeCard,
    assignCard,
    changingCard
}