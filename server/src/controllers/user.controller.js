const userModel = require('../models/user.model')
const mailService = require('../service/nodemailer.service')
const jwtMiddleware = require('../middleware/jwt.middleware')
const emailConfirmed = require('../notifications/emailConfirmed')

const createUser = (req, res) => {
    const { name, email, password, confirmPassword } = req.body
    const newUser = new userModel({name, email, password})
    
    if(password !== confirmPassword) return res.status(500).send({status: res.statusCode, message: 'Password and Confirm Password must match.'})

    // save user to database, send activation email and autheticate it
    newUser.save()
        .then(() => authenticateUser(req, res))
        .catch(err => res.status(500).send({status: res.statusCode, message: 'user already exists or bad email.'}))
}

const authenticateUser = (req, res) => {
    const { email, password } = req.body

    // fetch user and test password verification
    userModel.findOne({ email }, (err, user) => {
        if (err) throw err

        if (!user) return res.status(500).send({status: res.statusCode, message: `no account with ${email}.`})

        // test a matching password
        user.comparePassword(password, (err, isMatch) => {
            if (err) return res.status(500).send({status: res.statusCode, err})

            // when password fail return a incorrect password msg
            if(!isMatch) return res.status(404).send({status: res.statusCode, message: 'the email or password is incorrect.'})

            const userRes = {
                id: user._id,
                name: user.name,
                email: user.email,
                emailConfirmed: user.emailConfirmed
            }

            jwtMiddleware.createToken(userRes)
                .then(token => {
                    // generate token access, send email and send user data
                    if(!user.emailConfirmed) mailService(user.name, user.email, token, true)

                    return res.status(200).send({ user: userRes, token })
                })
        })
    })
}

const confirmUserEmail = (req, res) => {
    const { token } = req.params

    jwtMiddleware.decodeToken(token)
        .then(tokenDecoded => {
            userModel.findOneAndUpdate({email: tokenDecoded.email}, {emailConfirmed: true}, (err, user) => {
                if(err) throw err

                return res.status(200).send(emailConfirmed)
            })
        })
        .catch(err => res.status(500).send({status: res.statusCode, message: err.message}))
}

const recoverPassword = (req, res) => {
    const { email } = req.params

    // find user by email, if there is no one, catch it
    userModel.findOne({email}, '-password', (err, user) => {
        if(err) throw err

        if(!user) return res.status(404).send({status: res.statusCode, message: `no email registered with ${email}.`})
            
        const userRes = {
            id: user._id,
            name: user.name,
            email: user.email,
            emailConfirmed: user.emailConfirmed
        }

        jwtMiddleware.createToken(userRes)
            .then(token => {
                
                // send email with token
                mailService(user.name, user.email, token, false)

                // send positive message
                return res.status(200).send({status: res.statusCode, message: `please ${user.name}, check your email and click link to reset the password.`})
            })
    })
}   

const resetPassword = (req, res) => {
    const { email, password } = req.body

    jwtMiddleware.decodeToken(req.params.token)
        .then(tokenDecoded => {
            const userDecoded = tokenDecoded

            if(email !== userDecoded.email) return res.status(428).send({message: 'the email is not the same as the sent with link reset password.'})

            userModel.findOne({email: userDecoded.email}, (err, user) => {
                if(err) throw err

                user.set({ password })
                user.save((err, updateUser) => {
                    if(err) throw err

                    const userRes = {
                        id: user._id,
                        name: user.name,
                        email: user.email,
                        emailConfirmed: user.emailConfirmed
                    }

                    return res.status(200).send({user: userRes, token: req.params.token})
                })
            })
        })
        .catch(err => res.status(401).send({status: res.statusCode, message: err.message}))
}

module.exports = {
    createUser,
    authenticateUser,
    confirmUserEmail,
    recoverPassword,
    resetPassword
}