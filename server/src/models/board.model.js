const mongoose = require('mongoose')
const Schema = mongoose.Schema
const laneModel = require('./lane.model')

const boardSchema = new Schema({
    boardName: {
        type: String,
        default: '',
        maxlength: 25,
        required: true
    },
    lanes:  [{
        type: Schema.Types.ObjectId,
        ref: 'Lane'
    }],
    author: {
        type: Schema.Types.ObjectId,
        ref: 'User'
    },
    guests: [{
        type: Schema.Types.ObjectId,
        ref: 'User'
    }]
})

boardSchema.post('remove', function(boardDeleted) {
        let promises = []

        boardDeleted.lanes.forEach( laneId => promises.push( Promise.resolve( laneModel.findByIdAndRemove({_id: laneId}) ) ))

        Promise.all(promises)
            .catch( err => console.log(err.message))
})

module.exports = mongoose.model('Board', boardSchema)