const mongoose = require('mongoose')
const Schema = mongoose.Schema
const laneModel = require('./lane.model')

const cardSchema = new Schema({
    name: {
        type: String,
        required: true
    },
    completed: {
        type: Boolean,
        default: false
    },
    editName: {
        type: Boolean,
        default: false
    },
    assignedTo: {
        type: Schema.Types.ObjectId,
        ref: 'User',
        default: null
    }
})

cardSchema.post('remove', function(deletedCard) {
    laneModel.findOneAndUpdate({tasks: deletedCard._id}, {$pull: {tasks: deletedCard._id}}, (err, res) => {
        if(err) throw err

        return
    })
})

module.exports = mongoose.model('Card', cardSchema)