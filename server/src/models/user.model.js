const mongoose = require('mongoose')
const Schema = mongoose.Schema
const bcrypt = require('bcrypt')

const saltRounds = 10

const userSchema = new Schema({
    name: {
        type: String,
        require: true
    },
    email: {
        type: String,
        match: /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/,
        unique: true,
        require: true
    },
    emailConfirmed: {
        type: Boolean,
        default: false
    },
    password: {
        type: String,
        require: true
    }
})

userSchema.pre('save', function (next) {
    let user = this

    // only hash the password if it has been modified (or is new)
    if (!user.isModified('password')) return next()
    
    // generate a salt
    bcrypt.genSalt(saltRounds, (err, salt) => {
        if (err) return next(err)
    
        // hash the password along with our new salt
        bcrypt.hash(user.password, salt, (err, hash) => {
            if (err) return next(err)
    
            // override the cleartext password with the hashed one
            user.password = hash

            next()
        })
    })
})

userSchema.methods.comparePassword = function (candidatePassword, cb) {
    bcrypt.compare(candidatePassword, this.password, (err, isMatch) => {
        if (err) return cb(err)

        cb(null, isMatch)
    })
}

module.exports = mongoose.model('User', userSchema)