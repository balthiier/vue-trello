const mongoose = require('mongoose')
const Schema = mongoose.Schema

const laneSchema = new Schema({
    laneName: {
        type: String,
        default: '',
        required: true
    },
    editLaneText: {
        type: Boolean,
        default: false
    },
    editAddNewCard: {
        type: Boolean,
        default: false
    },
    tasks:  [{
        type: Schema.Types.ObjectId,
        ref: 'Card'
    }]
})

module.exports = mongoose.model('Lane', laneSchema)