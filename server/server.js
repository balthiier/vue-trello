const express = require('express')
const app = express()
const { port } = require('./src/config/server.config')
const quotes = require('./src/config/gandalf.quotes')

// importing express configs and app usage
require('./src/config/express.config')(app)
// importing mongoose connection's instance
require('./src/config/mongoose.config')

// initial route to check server status
app.get('/', (req, res) => {
    res.send({'server': 'OK', 'status': res.statusCode, 'date': new Date(), 'vue-trello': 'v1.0', 'message': quotes[Math.ceil(Math.random() * (quotes.length)) - 1]})
})

app.listen(port, () => console.log(`nodeJS process ${process.pid} running at port ${port}`))