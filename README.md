# vue-trello

> A project for the test to fullstack developer role at DevSquad, open two browsers and create two accounts, then start to use the application.

## Run mongoDB (must install and config for your own)
``` bash
mongod
```

## Start Front End

``` bash
# enter client folder
cd client

# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev
```

## Start Application with Node.js
``` bash
#enter server folder
cd server

#install dependencies
npm install

#run node application
npm run start(if you have nodemon then, $ nodemon server)

```

All process development was using Windows 10 OS, any problem, ask me, thanks.