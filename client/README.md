# vue-trello

> A project for the test to fullstack developer role at DevSquad.

## Build Setup To deploy VUE.JS

``` bash
# enter client folder
cd client

# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev

# build for production with minification
npm run build

# build for production and view the bundle analyzer report
npm run build --report

# run unit tests
npm run unit

# run all tests
npm test
```

## Start Application with Node.js
``` bash
#enter server folder
cd server

#install dependencies
npm install

#run node application
npm run start

```