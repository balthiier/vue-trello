import Vue from 'vue'
import Router from 'vue-router'

import UserIndex from '@/components/scene/Board/UserIndex'
import Board from '@/components/scene/Board/Board'
import SelectionBoard from '@/components/scene/Board/SelectionBoard'
import Registration from '@/components/scene/Registration/Registration'
import ForgotPass from '@/components/scene/ForgotPass/ForgotPass'
import ResetPass from '@/components/scene/ResetPass/ResetPass'
import Login from '@/components/scene/Login/Login'

import userInfo from '../methods/userInfo'
import Vuetify from 'vuetify'
import 'vuetify/dist/vuetify.min.css'

Vue.use(Router)
Vue.use(Vuetify)

const router = new Router({
  mode: 'history',
  routes: [
    {
      path: '/user/',
      component: UserIndex,
      meta: { requiresAuth: true },
      children: [
        {
          path: '',
          name: 'SelectionBoard',
          component: SelectionBoard,
          meta: { requiresAuth: true }
        },
        {
          path: 'board/:id',
          name: 'Board',
          component: Board,
          meta: { requiresAuth: true }
        },
      ]
    },
    {
      path: '/signup',
      name: 'Registration',
      component: Registration
    },
    {
      path: '/forgotpass',
      name: 'ForgotPass',
      component: ForgotPass
    },
    {
      path: '/resetpass/:token',
      name: 'ResetPass',
      component: ResetPass
    },
    {
      path: '/signin',
      name: 'Login',
      component: Login
    },
    {
      path: '*',
      redirect: '/signin'
    }
  ]
})

// router middleware to check if user is cached on localStorage
router.beforeEach((to, from, next) => {
  if(to.matched.some(record => record.meta.requiresAuth)) {
    const user = userInfo.getUserData()
    if(user) {
      return next() 
    }

    return next({ path: '/signin' })
  } else {
    next()
  }
})

export default router