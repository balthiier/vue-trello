const userInfo = {
    saveUserInfo: function(response) {
        localStorage.setItem('user', JSON.stringify(response))
        
        return
    },
    getUserData: function() {
        return JSON.parse(localStorage.getItem('user'))
    },
    clearUserData: function() {
        localStorage.removeItem('user')
    }
}

export default Object.create(userInfo)